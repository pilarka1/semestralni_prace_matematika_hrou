#include <QApplication> //Samotna aplikace @
#include <QLabel> //Textove pole - nelze zde klikat
#include <QFont> //Moznost upravy textu
#include <QPushButton> //Vsechny tlacitka
#include <QHBoxLayout> //Grafika - rovnání tlacitek horizontalne
#include <QVBoxLayout> //Grafika
#include <QObject> //Umozni reagovat na zmacknuti tlacitka @
#include <QSpinBox> //Policko s cislem (vyber)
#include <QString> //Vsechna prace s textem
#include <QStringList> //Seznam textu (seznam pismen - tajenka)
#include <QFileDialog> //Umozni to otevřít okno, kde lze vybrat soubory
#include <QComboBox> //Vyber z nekolika moznosti
#include <QGridLayout> //Vyrabi tabulku
#include <QInputDialog> //Okno pro vstup od uživatele
#include <QList> //Seznam polozek - generavani tlacitek!
#include <qmath.h> //Vsechny operace (matematicke)
#include <QPair> //vzorec a vysledek
#include <QRandomGenerator> //nahodne cisla
#include <QDialog>
#include <QMessageBox>
#include <QObject>
#include <QFile>
#include <QTextStream> 

int maxPocetPismenVTajence = 18;
int pocetVariant = 9;

class zpracovaniVtipu
{
private:
    /* data */
public:
    zpracovaniVtipu(QWidget *w);
    QString TextNadTajenkou;
    QStringList PismenkaVTajence;
    ~zpracovaniVtipu();
};

zpracovaniVtipu::zpracovaniVtipu(QWidget *w) //zde vlozit adresu souboru a on to da do formatu...
{
    QFile soubor=QFile(w);
    do
    {
        soubor.setFileName(QFileDialog::getOpenFileName(w, "Vybrat vtip", QDir::currentPath(), "(*.vtip)"));
    } while (!soubor.open(QFile::ReadOnly | QFile::Text));
    
    QTextStream streamVtipu=QTextStream (&soubor);
    PismenkaVTajence=streamVtipu.readAll().split(QLatin1Char('\n'));
    TextNadTajenkou=PismenkaVTajence.at(0);
    PismenkaVTajence.removeFirst();
    maxPocetPismenVTajence=PismenkaVTajence.size();
}

zpracovaniVtipu::~zpracovaniVtipu()
{
}

class vzorec
{
private:
    /* data */
public:
    QList<QPair<QString, int>> vzorecAVysledek; //QPair - prirazeni vzorce k vysledku, QList z toho udela seznam dvojic
    vzorec(int mod, int pocet);
    ~vzorec();
};

vzorec::vzorec(int mod, int pocet)
{
    int cislo1;
    int cislo2;
    QString vz; //vzorec
    int vysledek;
    int a=pocet/2;
    int b=pocet/3;
    //int c=pocet/6;

    switch (mod)
    {
    //Scitani a odcitani do 20
    case 1:
        for (int i = 0; i < a; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,10);
            cislo2 = QRandomGenerator::global()->bounded(0,10);
            vz = QString::number(cislo1)+ QString("+") + QString::number(cislo2)+QString("=");
            vysledek= cislo1+cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }

        for (int i = a; i < pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,20);
            cislo2 = QRandomGenerator::global()->bounded(0,20);
            if(cislo1<cislo2) //potreba osetrit, abych neodecitala treba 7-10 - nechci zaporna cisla
            {
                int p = cislo1;
                cislo1=cislo2;
                cislo2=p;
            }
            vz = QString::number(cislo1)+ QString("-") + QString::number(cislo2)+QString("=");
            vysledek= cislo1-cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        } 
        break;
         
    //Scitani a odcitani do 100
    case 2:
        for (int i = 0; i < a; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,50);
            cislo2 = QRandomGenerator::global()->bounded(10,50);
            vz = QString::number(cislo1)+ QString("+") + QString::number(cislo2)+QString("=");
            vysledek= cislo1+cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }

        for (int i = a; i < pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,100);
            cislo2 = QRandomGenerator::global()->bounded(0,100);
            if(cislo1<cislo2) //potreba osetrit, abych neodecitala treba 7-10 - nechci zaporna cisla
            {
                int p = cislo1;
                cislo1=cislo2;
                cislo2=p;
            }
            vz = QString::number(cislo1)+ QString("-") + QString::number(cislo2)+QString("=");
            vysledek= cislo1-cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }
        break;

     //Mala nasobilka
    case 3: 
        for (int i = 0; i < pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,5);
            cislo2 = QRandomGenerator::global()->bounded(0,5);
            vz = QString::number(cislo1)+ QString(".") + QString::number(cislo2)+QString("=");
            vysledek= cislo1*cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }
        break;

        //Velka nasobilka
        case 4: 
        for (int i = 0; i < pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,10);
            cislo2 = QRandomGenerator::global()->bounded(6,10);
            vz = QString::number(cislo1)+ QString(".") + QString::number(cislo2)+QString("=");
            vysledek= cislo1*cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }
        break;

        //Nasobeni - vse
        case 5: 
        for (int i = 0; i < pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,10);
            cislo2 = QRandomGenerator::global()->bounded(0,10);
            vz = QString::number(cislo1)+ QString(".") + QString::number(cislo2)+QString("=");
            vysledek= cislo1*cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }
        break;
     
     //deleni
     case 6: 
       
       for (int i = 0; i < pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,10);
            cislo2 = QRandomGenerator::global()->bounded(1,10); //nesmim delit 0
            int c1 = cislo1*cislo2; //Bylo potreba vyresit celociselne deleni
            vz = QString::number(c1)+ QString(":") + QString::number(cislo2)+QString("=");
            vzorecAVysledek.push_back(qMakePair(vz,cislo1));
        }
        break;

    //Nasobeni a deleni
    case 7:
     for (int i = 0; i < a; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,10);
            cislo2 = QRandomGenerator::global()->bounded(0,10);
            vz = QString::number(cislo1)+ QString(".") + QString::number(cislo2)+QString("=");
            vysledek= cislo1*cislo2;
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }
    
    for (int i = a; i < pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,10);
            cislo2 = QRandomGenerator::global()->bounded(1,10); //nesmim delit 0
            int c1 = cislo1*cislo2; //Bylo potreba vyresit celociselne deleni
            vz = QString::number(c1)+ QString(":") + QString::number(cislo2)+QString("=");
            vzorecAVysledek.push_back(qMakePair(vz,cislo1));
        }
        break;

    //Doplnovacka - nasobeni
    case 8:
     for (int i = 0; i <b; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(1,10);
            cislo2 = QRandomGenerator::global()->bounded(1,10);
            vysledek= cislo1*cislo2;
            vz = QString("_")+ QString(".") + QString::number(cislo2)+QString("=")+QString::number(vysledek);
            vzorecAVysledek.push_back(qMakePair(vz,cislo1));
        }
    
    for (int i = b; i <2*b; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(1,10);
            cislo2 = QRandomGenerator::global()->bounded(1,10);
            vysledek= cislo1*cislo2;
            vz = QString::number(cislo1)+ QString(".") + QString("_")+QString("=")+QString::number(vysledek);
            vzorecAVysledek.push_back(qMakePair(vz,cislo2));
        }
    
    for (int i = 2*b; i <pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(0,10);
            cislo2 = QRandomGenerator::global()->bounded(0,10);
            vysledek= cislo1*cislo2;
            vz = QString::number(cislo1)+ QString(".") + QString::number(cislo2)+QString("=")+QString("_");
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }
        break;

    //Doplnovacka - deleni
    case 9:
     for (int i = 0; i <b; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(1,10);
            cislo2 = QRandomGenerator::global()->bounded(1,10);
            vysledek= cislo1*cislo2;
            vz = QString("_")+ QString(":") + QString::number(cislo2)+QString("=")+QString::number(cislo1);
            vzorecAVysledek.push_back(qMakePair(vz,vysledek));
        }
    
    for (int i = b; i <2*b; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(1,10);
            cislo2 = QRandomGenerator::global()->bounded(1,10);
            vysledek= cislo1*cislo2;
            vz = QString::number(vysledek)+ QString(":") + QString("_")+QString("=")+QString::number(cislo1);
            vzorecAVysledek.push_back(qMakePair(vz,cislo2));
        }
    
    for (int i = 2*b; i <pocet; i++)
        {
            cislo1 = QRandomGenerator::global()->bounded(1,10);
            cislo2 = QRandomGenerator::global()->bounded(1,10);
            vysledek= cislo1*cislo2;
            vz = QString::number(vysledek)+ QString(":") + QString::number(cislo2)+QString("=")+QString("_");
            vzorecAVysledek.push_back(qMakePair(vz,cislo1));
        }
        break;

    default:
        break;
    }
}

vzorec::~vzorec()
{
}

class tajenka:public QWidget
{
private:
    /* data */
public:
    int *pocetChyb;
    QLabel *CisloPocetChyb;
    QList<QPushButton*> *tlacitkaTajenky;
    vzorec *priklad;
    zpracovaniVtipu *nacteniVtipu;
    QFont zprava;

    tajenka (QList<QPushButton*> *seznam,QLabel *textchyba,int *cisloChyba,vzorec *pr,zpracovaniVtipu *vtip)
    {
        tlacitkaTajenky=seznam;
        CisloPocetChyb=textchyba;
        pocetChyb=cisloChyba;
        priklad=pr;
        nacteniVtipu=vtip;
        zprava=QFont (" ",15);
        for (int i = 0; i < seznam->size(); i++)
        {
            connect(seznam->at(i),QPushButton::clicked,this,tajenka::info);
        }
    }
    public slots:
    void info()
    {
        QPushButton *b = dynamic_cast<QPushButton *>(sender());
        int i=b->objectName().toInt();
        bool ok;
        int j = QInputDialog::getInt(this, QString("Napiš výsledek"), priklad->vzorecAVysledek.at(i).first, 25, 0, 100, 1, &ok);
        if (ok)
        {
            if(j==priklad->vzorecAVysledek.at(i).second)
            {
                tlacitkaTajenky->at(i)->setText(nacteniVtipu->PismenkaVTajence.at(i)); //chci i te tlacitko, a tohoto tlacitka nastavim text
                QMessageBox spravne;
                spravne.setText("Správně");
                spravne.setStyleSheet("color: green");
                spravne.setFont(zprava);
                spravne.exec();
            }
            else
            {
                QMessageBox spatne;
                spatne.setText("Špatně - zkus to znovu");
                spatne.setStyleSheet("color: red");
                spatne.setFont(zprava);
                spatne.exec();
                (*pocetChyb)++;
                CisloPocetChyb->setNum(*pocetChyb);
            }
        }
    }
};


int main(int argc, char ** argv)
{
    int mod = 0;
    int pocetChyb =0; //Pocitani chyb
    QApplication app (argc,argv);
    QWidget w;
    QFont chyby(" ",12);
    QFont FontTextNadTajenkou(" ",14);
    //QPushButton vytvorVtip("Vytvoř si vlastní vtip");

    QLabel text ("Zvolte typ příkladu");
    QLabel textPocetChyb;
    QLabel CisloPocetChyb;
    text.setAlignment(Qt::AlignCenter);
    
    textPocetChyb.setText("Počet chyb:");
    textPocetChyb.setAlignment(Qt::AlignRight);
    textPocetChyb.setFont(chyby);

    CisloPocetChyb.setNum(pocetChyb);
    CisloPocetChyb.setFont(chyby);

    zpracovaniVtipu nacteniVtipu = zpracovaniVtipu (&w);

    QStringList items;
    items << QString("Sčítání a odčítání do 20")<< QString("Sčítání a odčítání do 100")<< QString("Malá násobilka")
    << QString("Velká násobilka")<< QString("Malá a velká násobilka")<< QString("Dělení")<< QString("Násobení a dělení")
    <<QString("Doplňovačka - násobení")<<QString("Doplňovačka - dělení");

    bool ok;
    QString item = QInputDialog::getItem(&w, QString("Zvol typ příkladů"),QString("Typ příkladů:"), items, 0, false, &ok);
    if (ok && !item.isEmpty())
    {
        for(int i=0;i<pocetVariant;i++)
        {
            if(item==items.at(i))
            {
                mod=i+1;
            }
        }
    }

    vzorec priklad = vzorec(mod,maxPocetPismenVTajence); //mod mi urcuje typ prikladu

    QLabel PrvniCastVtipu; 
    PrvniCastVtipu.setText(nacteniVtipu.TextNadTajenkou); //Vlozeni textu
    PrvniCastVtipu.setAlignment(Qt::AlignCenter);
    PrvniCastVtipu.setFont(FontTextNadTajenkou);
     
    QList<QPushButton *> tlacitkaTajenky = w.findChildren<QPushButton *>();
    
    QHBoxLayout TvarTajenky;

    for (int i = 0; i < maxPocetPismenVTajence; i++)
    {
        tlacitkaTajenky.push_back(new QPushButton(&w));
        tlacitkaTajenky.at(i)->setText(priklad.vzorecAVysledek.at(i).first);//chci i te tlacitko, a tohoto tlacitka nastavim text
        TvarTajenky.addWidget(tlacitkaTajenky.at(i));
        tlacitkaTajenky.at(i)->setObjectName(QString::number(i));
        tlacitkaTajenky.at(i)->setFixedSize(62,25);
    }

    //Rozlozeni
    QHBoxLayout hbox;
    QVBoxLayout vbox;
    QHBoxLayout cbox;
    cbox.addWidget(&textPocetChyb);
    cbox.addWidget(&CisloPocetChyb);
    hbox.addLayout (&TvarTajenky);
    vbox.addWidget(&PrvniCastVtipu);
    vbox.addLayout(&hbox);
    vbox.addLayout(&cbox);


    w.setLayout(&vbox);  
    w.setWindowTitle("Matematika hrou");

    tajenka kliknutiTajenka=tajenka(&tlacitkaTajenky,&CisloPocetChyb,&pocetChyb,&priklad,&nacteniVtipu);

    w.show();

    return app.exec();
}
