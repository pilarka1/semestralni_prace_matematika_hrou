#include <QApplication> //Samotna aplikace @
#include <QLabel> //Textove pole - nelze zde klikat
#include <QPushButton> //Vsechny tlacitka
#include <QVBoxLayout> //Grafika
#include <QObject> //Umozni reagovat na zmacknuti tlacitka @
#include <QString> //Vsechna prace s textem
#include <QFileDialog> //Umozni to otevřít okno, kde lze vybrat soubory
#include <QInputDialog> //Okno pro vstup od uživatele
#include <QDialog>
#include <QFile>
#include <QLineEdit>

int main(int argc, char ** argv)
{
    QApplication a(argc, argv);
    QWidget w;
    QLabel prvniRadek("Vlož zobrazovanou část vtipu",&w);
    QLineEdit viditelnaCastVtipu(" ",&w);
    QLabel tajenka("Vlož skrytou část vtipu.",&w);
    QLineEdit skrytaCastVtipu(" ",&w);
    QPushButton uloz("Uložit");

    QObject::connect(&uloz, QPushButton::clicked, [&](){
        QString text=viditelnaCastVtipu.text();
        QString text2=skrytaCastVtipu.text();
        if (text2.at(0)==' ')
        {
           text2.remove(0,1);
        }
        
        if (text.at(0)==' ')
        {
           text.remove(0,1);
        }
        text2=text2.replace(QString(),QString('\n'));
        if(text2.back()=='\n')
        {
            text2.remove(text2.size()-1,1);
        }
        
        text+=text2;
        QFile soubor=QFile(QFileDialog::getSaveFileName(&w, "Uložit", QDir::currentPath(), "(*.vtip)"),&w);
        soubor.open(QIODevice::WriteOnly | QIODevice::Text);
        soubor.write(text.toUtf8());
    });

    QVBoxLayout vbox;
    vbox.addWidget(&prvniRadek);
    vbox.addWidget(&viditelnaCastVtipu);
    vbox.addWidget(&tajenka);
    vbox.addWidget(&skrytaCastVtipu);
    vbox.addWidget(&uloz);

    w.setLayout(&vbox);


    w.show();
    return a.exec();
}

